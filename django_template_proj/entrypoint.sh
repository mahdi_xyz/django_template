#!/bin/bash

set -e

# pycharm dose not support venv in docker containner
# . /opt/venv/django_template_proj/bin/activate && \

python3 is_service_up/is_service_up.py && \

#--clear
python3 manage.py collectstatic --noinput && \

#python3 manage.py flush --no-input && \
python3 manage.py makemigrations && \
python3 manage.py migrate && \

# chgrp -R "$DJANGO_GROUP" "$DJANGO_PROJECT" && \
chmod -R u=rwx,g=rx,o= "$DJANGO_PROJECT" && \

#su "$DJANGO_USER" -c "set -e; . $DJANGO_PROJECT_BASE/venv/$DJANGO_PROJECT_NAME/bin/activate; cd $DJANGO_PROJECT;  python manage.py runserver"


exec "$@"
