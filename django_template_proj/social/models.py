from django.db import models, transaction
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site

USER_MODEL = get_user_model()


class SocialProvider(models.Model):
    SENTINEL = 'sentinel'
    GOOGLE = 'google'
    GITHUB = 'github'
    Social_List = [
        (None, 'Select Provider'),
        (GOOGLE, 'Google'),
        (GITHUB, 'Github'),

        # for deleted provider of SocialAccount instances
        (SENTINEL, 'sentinel'),
    ]
    social = models.CharField(
        unique=True,
        max_length=10,
        choices=Social_List,
        null=False,
        blank=False, )

    client_id = models.CharField(blank=False, null=False, max_length=1000)

    def __str__(self):
        return self.social

    class Meta:
        verbose_name = 'Social Provider'
        verbose_name_plural = 'Social Providers'


def get_sentinel_social_provider():
    """return sentinel object of SocialProvider Model, if does not exists create and return"""
    return SocialProvider.objects.get_or_create(social=SocialProvider.SENTINEL, defaults={'client_id': ''})[0]


class SocialAccount(models.Model):
    """ users who have a social account have their records here, via foreign key to user model"""

    # if site is going to be deleted the social account connected with it will be deleted to, !!!
    site = models.ForeignKey(to=Site, on_delete=models.CASCADE, null=False, blank=False, default=settings.SITE_ID)
    # related_name = "socialaccount_site",)

    user = models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=False, blank=False, )
    # related_name="socialaccount_user")

    # on_delete: do Not delete social accounts on deleting a provider
    provider = models.ForeignKey(
        to=SocialProvider, on_delete=models.SET(get_sentinel_social_provider),
        blank=False, null=False)
    # the user identifier in their social account
    social_provider_identifier = models.CharField(blank=False, null=False, max_length=1000, )
    is_connected = models.BooleanField(null=False, blank=False, default=False, )

    email = models.EmailField(blank=False, null=True, max_length=1000, unique=True, )

    def __str__(self):
        return "SOCIAL_EMAIL: %s::%s::%s" % (self.email, self.provider.social, self.user.email,)

    class Meta:
        verbose_name = 'Social Account'
        verbose_name_plural = 'Social Accounts'
        # ensures for a user there is only one account per provider and per site at most
        constraints = [
            models.UniqueConstraint(fields=['user', 'provider', 'site'],
                                    name='user have one account per provider and site'),
            models.UniqueConstraint(fields=['social_provider_identifier', 'provider', 'site'],
                                    name='every social_provider_identifier per provider is unique for every site'),
        ]


@transaction.atomic
def create_social_create_email_user(social_id, email, provider, is_email_verified=False, ):
    email_user, is_created = USER_MODEL.objects.get_or_create(
        email=email,
        defaults={
            'is_email_verified': is_email_verified}, )

    if not is_created:
        # raise and the transaction cause rollback
        raise ValueError("EmailUser already exist PK: %d" % email_user.pk)

    social_account, is_created = SocialAccount.objects.get_or_create(
        site=Site.objects.get_current(),
        user=get_user_model().objects.get(email=email),
        provider=SocialProvider.objects.get(
            social=provider),
        defaults={
            'social_provider_identifier': social_id, 'is_connected': True,
            'email': email, })

    if not is_created:
        # raise and the transaction cause rollback
        raise ValueError("SocialAccount already exist PK: %d" % social_account.pk)
    return email_user, social_account


@transaction.atomic
def add_social_account_to_user(user, social_id, social_provider, email):
    SocialAccount.objects.create(site=Site.objects.get_current(), user=user, provider=SocialProvider.objects.get(
        social=social_provider), social_id=social_id, isConnected=True, email=email, )
