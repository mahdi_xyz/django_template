module.exports = {
  publicPath: "/static/frontend/dist/",
  outputDir: "./static/frontend/dist/",
  assetsDir: './asset/',
  filenameHashing: true,

  configureWebpack: {


    output: {
      filename: '[name].bundle.js',
      chunkFilename: '[id].bundle.js',
    },

    module: {
      rules: [
        // {
        //   test: /\.(png|woff|woff2|svg|eot|ttf|gif|jpe?g)$/,
        //   use: [
        //     {
        //       loader: 'url-loader',
        //       options: {
        //         limit: 1000,
        //         // ManifestStaticFilesStorage reuse.
        //         // name: '[path][name].[md5:hash:hex:12].[ext]',
        //         name: '[name].[md5:hash:hex:12].[ext]'
        //       },
        //     },
        //   ],
        // },
      ]
    }
  },


  css: {
    // Enable CSS source maps.
    sourceMap: process.env.NODE_ENV !== "production",

    extract: {
      filename: "[name].bundle.css",
      chunkFilename: "[id].bundle.css"
    }
    // loaderOptions: {
    //   css: {
    //     url: true,
    //     modules: false,
    //   },

  },
  devServer: {
    proxy: 'http://127.0.0.1:8080',
    // watchOptions: { aggregateTimeout: 300, poll: 1000 },
    headers: {
      "Access-Control-Allow-Origin": "http://127.0.0.1:8080",
    }
  }
};
