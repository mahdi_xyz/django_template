import Vue from 'vue';
import Router from 'vue-router';
import Index from './pages/Index.vue';
// import Landing from './pages/Landing.vue';
import MyLanding from "./pages/MyLanding";
import Login from './pages/Login.vue';
// import Profile from './pages/Profile.vue';
import MainNavbar from './layout/MainNavbar.vue';
import MyMainNavbar from './layout/MyMainNavbar';
import MainFooter from './layout/MainFooter.vue';
import MyMainFooter from "./layout/MyMainFooter";
import MyProfile from "./pages/MyProfile";

Vue.use(Router);

export default new Router({
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'index',
      components: {default: Index, header: MainNavbar, footer: MainFooter},
      props: {
        header: {colorOnScroll: 400},
        footer: {backgroundColor: 'black'}
      }
    },
    {
      path: '/landing',
      name: 'landing',
      components: {
        default: () => import(/* webpackPrefetch: true,  webpackChunkName: "group-aaa" */ "./pages/MyLanding"),
        header: () => import(/* webpackChunkName: "group-aaa" */ "./layout/MyMainNavbar.vue"),
        footer: () => import(/* webpackChunkName: "group-aaa" */ "./layout/MyMainFooter")
      },
      props: {
        header: {colorOnScroll: 400},
        footer: {backgroundColor: 'black'}
      }
    },
    {
      path: '/login',
      name: 'login',
      components: {
        default: () => import(/* webpackChunkName: "group-aaa" */ "./pages/MyLogin.vue"),
        header: () => import(/* webpackChunkName: "group-aaa" */ "./layout/MyMainNavbar.vue"),
      },
      props: {
        header: {colorOnScroll: 400}
      }
    },
    {
      path: '/profile',
      name: 'profile',
      components: {
        default: () => import("./pages/MyProfile.vue"), header: () => import("./layout/MyMainNavbar.vue"),
        footer: () => import("./layout/MyMainFooter")
      },
      props: {
        header: {colorOnScroll: 400},
        footer: {backgroundColor: 'black'}
      }
    }
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return {selector: to.hash};
    } else {
      return {x: 0, y: 0};
    }
  }
});
