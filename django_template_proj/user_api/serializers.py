from django.contrib.auth import get_user_model, authenticate
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.serializers import ModelSerializer
from rest_framework.validators import UniqueValidator

UserModel = get_user_model()


class CreateUserSerializer(serializers.ModelSerializer):
    """Serializer for the users object"""
    error_messages = {
        'password_mismatch': _('The two password fields did NOT match.'),
    }
    password1 = serializers.CharField(
        label=_("Password"),
        trim_whitespace=False,
        required=False,
        write_only=True,
        min_length=8,
        help_text=_("Enter the password for your account."),
    )
    password2 = serializers.CharField(
        label=_("Password confirmation"),
        trim_whitespace=False,
        required=False,
        write_only=True,
        min_length=8,
        help_text=_("Enter the same password again, for verification."),
    )
    email = serializers.EmailField(label='Email', max_length=254, required=True,
                                   validators=[UniqueValidator(queryset=UserModel.objects.all(),
                                                               message='Unable to sign in with provided credentials.')])

    class Meta:
        model = UserModel
        fields = ('email', 'password1', 'password2', 'first_name', 'last_name')
        extra_kwargs = {'password': {'write_only': True, 'min_length': 8, 'required': True}, }

    def validate(self, attrs):
        if hasattr(attrs, "password1") and hasattr(attrs, "password2"):
            password1 = attrs.get("password1")
            password2 = attrs.get("password2")
            if password1 != password2:
                raise serializers.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        return attrs

    def create(self, validated_data):
        """ Create a new user with encrypted password and return it """
        validated_data.pop('password1', None)
        password = validated_data.pop('password2', None)
        return UserModel.objects.create_user(password=password, **validated_data)

    def update(self, instance, validated_data):
        """ Update a user, setting the password correctly and return it """
        validated_data.pop('password1', None)
        password = validated_data.pop('password2', None)
        user = super().update(instance, validated_data)

        if password:
            user.set_password(password)
            user.save()

        return user


class DynamicFieldsModelSerializer(CreateUserSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)
        fields_extra_kwargs = kwargs.pop('fields_extra_kwargs', None)

        # Instantiate the superclass normally
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields)
            print('allowed:', allowed)
            print('existing:', existing)
            for field_name in existing - allowed:
                print('field will remove:', field_name)
                self.fields.pop(field_name)


class EmailAuthTokenSerializer(AuthTokenSerializer):
    username = None
    email = serializers.EmailField(label=_("Email"), min_length=5, style={'input_type': 'email'}, write_only=True)
    token = serializers.CharField(label=_("Token"), style={'input_type': 'password'}, read_only=True)

    class Meta:
        fields = ('__all__',)
        extra_kwargs = {'password': {'write_only': True}, }

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            user = authenticate(request=self.context.get('request'),
                                username=email, password=password)

            # The authenticate call simply returns None for is_active=False
            # users. (Assuming the default ModelBackend authentication
            # backend.)
            if not user:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = _('Must include "email" and "password".')
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs
