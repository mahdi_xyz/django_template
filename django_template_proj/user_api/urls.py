from django.urls import path
from user_api import views

app_name = 'user-api'

urlpatterns = [
    path('create/', views.CreateUserView.as_view(), name='create'),
    path('retrieve-update-deactivate/', views.RetrieveUpdateDestroyUserView.as_view(), name='retrieve-update-deactivate'),
    path('deactivate/', views.DeactivateUserView.as_view(), name='deactivate'),

    path('token/', views.CreateTokenView.as_view(), name='token'),
]
# user-api:create