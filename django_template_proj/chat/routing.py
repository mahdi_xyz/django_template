from django.urls import re_path

from . import consumers

websocket_urlpatterns = [
    re_path(r'chat/chat-room/(?P<room_name>\w+)/$', consumers.ChatConsumer),
]
