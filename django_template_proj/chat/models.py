from django.db import models
from django.contrib.auth import get_user_model

UserModel = get_user_model()


class Groups(models.Model):
    name = models.fields.CharField(unique=True, null=False, max_length=120)
    members = models.ManyToManyField(to=UserModel, related_name='group_members')
    timeline = models.ManyToManyField(to="TimeLine", through='GroupTimeLineSender',
                                      through_fields=('group', 'timeline'),
                                      related_name='group_timeLine')

    @staticmethod
    def create_or_pass(group_name):
        """if group is not created it will create it
        otherwise will pass"""
        if not Groups.objects.filter(name=group_name).exists():
            Groups.objects.create(name=group_name)


def group_directory_path(instance):
    # file will be uploaded to MEDIA_ROOT/group_<name>/user_<id>/<filename>
    return 'group_{0}_user_{1}/%Y/%m/%d'.format(instance.chat_group.group_name, instance.sender.id, )


class TimeLine(models.Model):
    content = models.fields.TextField(null=True, blank=True, max_length=2000)
    # binary = models.fields.BinaryField(null=True, blank=False, max_length=20000)
    file = models.FileField(upload_to=group_directory_path)

    # this only set once
    time_created = models.fields.DateTimeField(auto_now_add=True)

    # auto_now: updated with .save() only
    # for QuerySet.update(): the value for last_modified must be added
    last_modified = models.fields.DateTimeField(auto_now=True)

    # TODO make sentinel  on delete and null=False
    sender = models.ForeignKey(to=UserModel, on_delete=models.SET_NULL, null=True)

    class Meta:
        ordering = ["-time_created"]
        get_latest_by = ["-time_created"]  # TimeLine.objects.latest() time_created become default argument
        verbose_name_plural = "chat room time line"

    @staticmethod
    def last_10_messages(group_name):
        try:
            return Groups.objects.get(name=group_name).timeline.all()[:10]
        except Groups.DoesNotExist:
            return None

    @staticmethod
    def save_message_group(group_name, sender, content=None, file=None):
        try:
            sender = UserModel.objects.get(email=sender)
            return Groups.objects.get(name=group_name).timeline.create(content=content, file=file, sender=sender)
        except (Groups.DoesNotExist, UserModel.DoesNotExist) as err:
            return None


class GroupTimeLineSender(models.Model):
    group = models.ForeignKey(to=Groups, on_delete=models.SET_NULL, null=True)
    timeline = models.OneToOneField(to=TimeLine, on_delete=models.SET_NULL, null=True)

    # timeline = models.ForeignKey(to=TimeLine, on_delete=models.SET_NULL, null=True, unique=True)

    class Meta:
        ordering = ["group"]
