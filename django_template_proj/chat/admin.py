from django.contrib import admin
from .models import Groups, TimeLine

admin.site.register(Groups)
admin.site.register(TimeLine)
