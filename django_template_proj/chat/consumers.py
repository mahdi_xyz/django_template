import json

from channels.generic.websocket import AsyncWebsocketConsumer
from asgiref.sync import sync_to_async, async_to_sync
from channels.db import database_sync_to_async
from .models import TimeLine, Groups


class ChatConsumer(AsyncWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        try:
            super().__init__(*args, **kwargs)

            self.username = self.room_group_name = self.room_name = ''
        except Exception as e:
            print('error:', e)

    async def connect(self):
        print('connect')
        try:
            self.room_name = self.scope['url_route']['kwargs']['room_name']
            self.room_group_name = 'chat_%s' % self.room_name

            # TODO any wayto prevent if group exists on each connect event
            await database_sync_to_async(self.wrap_create_or_pass)()

            # get username from AUTH MIDDLEWARE
            username = 'anonymous'
            if not self.scope['user'].is_anonymous:
                username = self.scope['user'].email
            self.username = username

            await self.accept()
        except Exception as e:
            print('error:', e)

    async def disconnect(self, close_code):
        print('disconnect')
        try:
            # Leave room group
            await self.channel_layer.group_discard(
                self.room_group_name,
                self.channel_name
            )
        except Exception as e:
            print('error:', e)

    # Receive message from WebSocket
    async def receive(self, text_data=None, bytes_data=None):
        print('receive:', text_data)
        try:
            text_data = json.loads(text_data)

            # from websocket.onmessage javascript client
            command = text_data.get('command', None)

            if command == 'chat.join':
                await self.chat_join()
                print('D_A_')

            if command == 'fetch.message':
                # await sync_to_async(self.fetch_message)()
                last_messages = await database_sync_to_async(self.wrap_last_10_messages)()
                await sync_to_async(self.fetch_message)(last_messages)
                print('D_B_')
            # from chat javascript client
            if command == 'chat.message':
                # Send message to room group
                await self.channel_layer.group_send(
                    self.room_group_name,
                    {
                        'type': 'chat.message',
                        'username': self.username,
                        'message': text_data.get('message', None)
                    }
                )
                await database_sync_to_async(self.wrap_save_message_group)(message=text_data.get('message', None))

            print('E___')

        except Exception as e:
            print('error:', e)
        print('F___')

    # Receive message from group_send method
    async def chat_message(self, event):
        print('chat_message:', event)

        try:
            # Send message to WebSocket
            await self.send(text_data=json.dumps(event))
        except Exception as e:
            print('error:', e)

    async def chat_join(self):
        """
            Called by receive when someone sent a join command.
        """
        print('join_room:', self.username)
        try:
            # Add them to the group so they get room messages
            await self.channel_layer.group_add(
                self.room_group_name,
                self.channel_name
            )
            print('A_____')
            # Send a join mesIFY_USERS_ON_ENTER_OR_LEAVE_ROOMS:
            # await self.channel_layer.group_send(
            #     self.room_group_name,
            #     {
            #         'type': 'chat.join',
            #         'username': self.username,
            #     }
            # )
            print('B_____')
            # Instruct client who make this connection (only one) to finish opening the room
            await self.send(json.dumps(
                {
                    'type': 'chat.join',
                    'room_name': self.room_group_name,
                    "username": self.username
                }))
        except Exception as e:
            print('error:', e)
        print('C_____')

    def wrap_create_or_pass(self):
        Groups.create_or_pass(self.room_group_name)

    def wrap_last_10_messages(self):
        return TimeLine.last_10_messages(self.room_group_name)

    def wrap_save_message_group(self, message):
        TimeLine.save_message_group(self.room_group_name, self.username, content=message, )

    def fetch_message(self, last_messages):
        print('fetch_message')
        try:
            # last_messages = await sync_to_async(self.wrap_last_10_messages)()
            print('last_messages:', last_messages)

            # in JSON object is an unordered collection :: dictionary in python
            # in JSON array is an ordered sequence :: dictionary in python
            if not last_messages.exists():
                async_to_sync(self.send)(
                    (json.dumps({
                        'type': 'fetch.message',
                        'room_name': self.room_group_name,
                        'username': self.username,
                        'last_messages': None
                    })))
                return

            last_messages_list_dict = []
            for message in last_messages:
                last_messages_list_dict.append({'room_name': message.group_timeLine.get(name=self.room_group_name).name,
                                                'username': message.sender.email,
                                                'message': message.content})

            async_to_sync(self.send)(json.dumps({
                'type': 'fetch.message',
                'room_name': self.room_group_name,
                'username': self.username,
                'last_messages': last_messages_list_dict
            }))
        except Exception as e:
            print('error:', e)
