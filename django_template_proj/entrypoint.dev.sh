#!/bin/bash

set -e

#python3 is_service_up/is_service_up.py && \

#python3 manage.py flush --no-input && \
python3 manage.py makemigrations && \
python3 manage.py migrate && \


exec "$@"
