from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings

from rest_framework import permissions, authentication
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title="Snippets API",
        default_version="v1",
        description="Test description",
        terms_of_service="https://",
        contact=openapi.Contact(email="mahdi.hamzeh.14@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    authentication_classes=(authentication.TokenAuthentication,),
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path("", include("vue_app.urls")),
    path("api/", include("vue_app.api.urls")),
    path("", include("user.urls")),
    path("api/user/", include("user_api.urls")),
    path("", include("google_api.urls")),
    path("chat/", include("chat.urls")),
    path("admin/", admin.site.urls),
    path("social-auth/", include("social_django.urls", namespace="social")),
]

urlpatterns += [
    re_path(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    re_path(
        r"^swagger/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    re_path(
        r"^redoc/$", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"
    ),
]

if settings.DEBUG:
    import debug_toolbar
    from django.conf.urls.static import static

    urlpatterns.append(path("__debug__/", include(debug_toolbar.urls)))
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
