import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
LOGS_DIR = os.path.join(BASE_DIR, 'django_template_proj', 'var', 'logs')
# DATABASE_DIR = os.path.join("/var/opt", os.environ.get("DJANGO_PROJECT_NAME"))
DATABASE_DIR = os.path.join(BASE_DIR, os.environ.get("DJANGO_PROJECT_NAME"), 'var', 'opt')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = os.environ.get("DJANGO_ALLOWED_HOSTS").split(" ")

# consider app for make migrations
# if template dir exists look for templates in app dir search start by first app in the list and leave with the first hit
# if static dir exists look for static files in app dir search start by first app in the list and leave with a hit
INSTALLED_APPS = [
    # channels overwrite default django runserver so must be first in installed_apps
    'channels',
    # for template resolution conflict give priority over django.contrib.admin app, by being before the app
    'user.apps.UserConfig',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    'rest_framework.apps.RestFrameworkConfig',
    'rest_framework.authtoken',

    'user_api.apps.UserApiConfig',
    'social.apps.SocialConfig',
    'google_api.apps.GoogleApiConfig',
    'chat',
    'vue_app',
    # 'debug_toolbar',
    'drf_yasg',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    # whitenoise package
    'whitenoise.middleware.WhiteNoiseMiddleware',
    # security alert! Over HTTPS ONLY !
    'django.middleware.gzip.GZipMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    # cache whole website
    # 'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.common.CommonMiddleware',
    # cache whole website
    # 'django.middleware.cache.FetchFromCacheMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'debug_toolbar.middleware.DebugToolbarMiddleware',
]

ROOT_URLCONF = 'django_template_proj.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'django_template_proj.wsgi.application'

# Channels
ASGI_APPLICATION = 'django_template_proj.routing.application'

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('redis', 6379)],
            # "hosts": [os.environ['REDIS_URL']],
        },
    },
}

# Debug-toolbar
# INTERNAL_IPS = [
#     '127.0.0.1',
#     # docker container Gateway
#     '172.18.0.1',
# ]

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },
    'mongo': {
        'ENGINE': 'djongo',
        'NAME': 'mongodb_django_template_proj',
        'ENFORCE_SCHEMA': False,
        'CLIENT': {
            'host': 'mongodb',
            'port': 27017,
            'username': 'root',
            'password': 'mongoadmin',
            'authSource': 'admin',
            'authMechanism': 'SCRAM-SHA-1'
        },
        'LOGGING': {
            'version': 1,
            'loggers': {
                'djongo': {
                    'level': 'DEBUG',
                    'propagate': False,
                }
            },
        },
    }
}

# todo??? Known Issues
# CONN_MAX_AGE other than zero is known to cause issues according to bug #4878.
# Until this is fixed, please set CONN_MAX_AGE to zero.

# DATABASES = {
#     "default": {
#         'ENGINE': os.environ.get("SQL_ENGINE", "django.db.backends.sqlite3"),
#         'NAME': os.environ.get("SQL_NAME", os.path.join(BASE_DIR, "db.sqlite3")),
#         'USER': os.environ.get("SQL_USER", "user"),
#         'PASSWORD': os.environ.get("SQL_PASSWORD", "password"),
#         'HOST': os.environ.get("SQL_HOST", "localhost"),
#         'PORT': os.environ.get("SQL_PORT", "5432"),
#     }
# }

# django-db-geventpool
# for django 1.6 and newer version, CONN_MAX_AGE must be set to 0, or connections will never go back to the pool
# DATABASES = {
#     'default': {
#         'ENGINE': 'django_db_geventpool.backends.postgresql_psycopg2',
#         'NAME': os.environ.get("SQL_NAME", os.path.join(BASE_DIR, "db.sqlite3")),
#         'USER': os.environ.get("SQL_USER", "user"),
#         'PASSWORD': os.environ.get("SQL_PASSWORD", "password"),
#         'HOST': os.environ.get("SQL_HOST", "localhost"),
#         'PORT': os.environ.get("SQL_PORT", "5432"),
#         'ATOMIC_REQUESTS': False,
#         'CONN_MAX_AGE': 0,
#         'OPTIONS': {
#             # heroku free dyno connections limit
#             'MAX_CONNS': 20
#         }
#     }
# }

if "DATABASE_URL" in os.environ:
    import dj_database_url

    # os.environ["DATABASE_URL"] += '?' + 'MAX_CONNS=20'
    # for django 1.6 and newer version, CONN_MAX_AGE must be set to 0, or connections will never go back to the pool
    DATABASES = {'default': dj_database_url.config(
        engine='django_db_geventpool.backends.postgresql_psycopg2',
        conn_max_age=0, ssl_require=True)}

    DATABASES['default']['ATOMIC_REQUESTS'] = False
    DATABASES['default']['OPTIONS']['MAX_CONNS'] = 20

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'frontend', 'static')
]
# will not if Debug=True
# Versioned files are cached forever, non-versioned files are cached for 60 seconds.
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

# whitenoise settings
WHITENOISE_KEEP_ONLY_HASHED_FILES = True

# Versioned files age (hashed usually by md5 algorithm)
WHITENOISE_MAX_AGE = 3600

# Media files (user upload images etc...)
MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "mediafiles")

# django.contrib.sites
SITE_ID = 1

# Custom user model
AUTH_USER_MODEL = 'user.EmailUser'

# Email Backends

# Email Console Backend
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# Email File Backend
EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = os.path.join(BASE_DIR, 'emails')

# Email Send Grid SMTP server
# SENDGRID_API_KEY = os.environ.get('SENDGRID_API_KEY')
#
# EMAIL_HOST = 'smtp.sendgrid.net'
# EMAIL_HOST_USER = 'apikey'
# EMAIL_HOST_PASSWORD = SENDGRID_API_KEY
# EMAIL_PORT = 587
# EMAIL_USE_TLS = True

# Email Send Grid via Web API
# requires: pip install django-sendgrid-v5
# EMAIL_BACKEND = "sendgrid_backend.SendgridBackend"
# SENDGRID_API_KEY = os.environ.get("SENDGRID_API_KEY")

# There are also optional settings to deliver emails in debug mode or to send them to standard output:
# Toggle sandbox mode (when running in DEBUG mode)
# SENDGRID_SANDBOX_MODE_IN_DEBUG = True

# echo to stdout or any other file-like object that is passed to the backend via the stream kwarg.
# SENDGRID_ECHO_TO_STDOUT = True

# social google app
GOOGLE_CLIENT_FILE_PATH = os.path.join(BASE_DIR, 'google_api', 'client_secret.json')
GOOGLE_OPTIONS = {
    'prompt': 'consent',
    'access_type': 'offline',
    'include_granted_scopes': 'true'
}

# celery::broker via rabbitmq
BROKER_USER = os.environ.get("BROKER_USER")
BROKER_PASSWORD = os.environ.get("BROKER_PASSWORD")
BROKER_HOST = os.environ.get("BROKER_HOST")
BROKER_PORT = os.environ.get("BROKER_PORT")
BROKER_VHOST = os.environ.get("BROKER_VHOST")

CELERY_BROKER_URL = f"amqp://{BROKER_USER}:{BROKER_PASSWORD}@{BROKER_HOST}:{BROKER_PORT}/{BROKER_VHOST}"
# CELERY_BROKER_URL = 'amqp://mahdi:mahdi@rabbitmq:5672/mahdi_vhost'

# celery::broker via redis
# CELERY_BROKER_URL = 'redis://localhost:6379'
# CELERY_RESULT_BACKEND = 'redis://redis:6379/0'

# Celery Data Format
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = TIME_ZONE

# CPU-bound tasks are best executed by a prefork execution pool.
# I/O bound tasks are best executed by a gevent/eventlet execution pool.
# both gevent/eventlet are base on greenlet (also known as green threads)
# CELERY_POOL = 'gevent'

CELERYD_TASK_SOFT_TIME_LIMIT = 12
CELERYD_TASK_TIME_LIMIT = 24
# celery -A django_template_proj.celery worker -n worker2@%h --loglevel=info -Q mail -P gevent
CELERY_TASK_QUEUES = {
    'mailing': {
        'exchange': 'mailing',
        'routing_key': 'mailing',
    },
}

# celery  routing and queues
CELERY_TASK_ROUTES = {
    'user.tasks._send_mail': {
        'queue': 'mailing',
        'routing_key': 'mailing',
        'exchange': 'mailing',
    }
}

# local memory cache
# CACHES = {
#     'default': {
#         'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
#         'LOCATION': 'DataFlair',
#     }
# }


DJANGO_REDIS_LOG_IGNORED_EXCEPTIONS = True

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": os.environ["REDIS_URL"],
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "IGNORE_EXCEPTIONS": True,
        }
    }
}

SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"

# Middleware cache
# redis cache
# CACHES = {
#     "default": {
#         "BACKEND": "django_redis.cache.RedisCache",
#         "LOCATION": "redis://" + "redis" + ":6379/1",
#         "TIMEOUT ": 10,
#         "OPTIONS": {
#             "CLIENT_CLASS": "django_redis.client.DefaultClient",
#             'MAX_ENTRIES': 1000,
#         },
#         "KEY_PREFIX": "redis_1"
#     }
# }

# CACHE_MIDDLEWARE_ALIAS = 'default'
# UpdateCacheMiddleware: sets Expires and Cache-Control of http header
# CACHE_MIDDLEWARE_SECONDS = 10
# for shared cache among same django installation
# CACHE_MIDDLEWARE_KEY_PREFIX = ''

# REST framework
# security: https only for Token and Session Authentication
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    )
}

# http header origin  allowed
ORIGINS = ['']

# CORS
CORS_ORIGIN_ALLOW_ALL = False

if DEBUG:
    CORS_ORIGIN_WHITELIST = [
        'http://localhost:8081',
    ]
else:
    CORS_ORIGIN_WHITELIST = ORIGINS

CORS_ALLOW_CREDENTIALS = True

# CSRF
CSRF_TRUSTED_ORIGINS = ['localhost:8081']

# change to app.example.com in production settings
CSRF_FAILURE_VIEW = 'vue_app.api.views.csrf_failure'

# Deployment Check List
# CSRF_COOKIE_SECURE = True
# SESSION_COOKIE_SECURE = True

#  Logs
# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': False,
#     'formatters': {
#         'simple': {
#             'format': '%(levelname)s %(message)s',
#         },
#
#         'verbose': {
#             'format': '%(asctime)s %(levelname)s [%(name)s:%(lineno)s] %(module)s %(process)d %(thread)d %(message)s',
#             'datefmt': "%Y/%m/%d %H:%M:%S"
#         },
#
#         'django.server': {
#             '()': 'django.utils.log.ServerFormatter',
#             'format': '[{server_time}] {message}',
#             'style': '{',
#         },
#     },
#     'handlers': {
#         # 'gunicorn_file_debug': {
#         #     'level': 'DEBUG',
#         #     'class': 'logging.handlers.RotatingFileHandler',
#         #     'formatter': 'verbose',
#         #     'filename': os.path.join(os.path.join(BASE_DIR, 'django_template_proj'), 'gunicorn.debug.log'),
#         #     'maxBytes': 1024 * 1024 * 100,  # 100 mb
#         # },
#         'default': {
#             'level': 'DEBUG',
#             'class': 'logging.StreamHandler',
#             'formatter': 'verbose'
#         },
#         'django': {
#             'level': 'DEBUG',
#             'class': 'logging.handlers.TimedRotatingFileHandler',
#             'filename': os.path.join(LOGS_DIR, 'django.log'),
#             # TimedRotatingFileHandler: Rotate log file daily, only keep 1 backup
#             'when': 'd',
#             'interval': 1,
#             'backupCount': 1,
#         },
#         'django.request': {
#             'level': 'DEBUG',
#             'class': 'logging.FileHandler',
#             'filename': os.path.join(LOGS_DIR, 'django.request.log'),
#         },
#         'django.server': {
#             'level': 'DEBUG',
#             'class': 'logging.FileHandler',
#             'filename': os.path.join(LOGS_DIR, 'django.server.log'),
#             'formatter': 'django.server',
#         },
#         # duo to sensitive data must be used with high security with package: Sentry
#         'mail_admins': {
#             'level': 'ERROR',
#             'class': 'django.utils.log.AdminEmailHandler',
#             'include_html': True,  # like DEBUG=True html
#             # overrides settings.EMAIL_BACKEND
#             'email_backend': 'django.core.mail.backends.filebased.EmailBackend',
#         },
#         'celery': {
#             'level': 'DEBUG',
#             'class': 'logging.handlers.RotatingFileHandler',
#             'filename': os.path.join(LOGS_DIR, 'celery.log'),
#             'formatter': 'verbose',
#             'maxBytes': 1024 * 1024 * 100,  # 100 mb
#         },
#     },
#     'loggers': {
#         # 'gunicorn.errors': {
#         #     'level': 'DEBUG',
#         #     'handlers': ['gunicorn_file_debug'],
#         #     'propagate': True,
#         # },
#         'django': {
#             'handlers': ['django'],
#             'level': 'DEBUG',
#             'propagate': True,
#         },
#         'django.request': {
#             'handlers': ['django.request'],
#             'level': 'DEBUG',
#             'propagate': True,
#         },
#         # manage.py runserver
#         'django.server': {
#             'handlers': ['django.server'],
#             'level': 'DEBUG',
#             'propagate': False,
#         },
#         'celery': {
#             'handlers': ['celery', 'default'],
#             'level': 'DEBUG',
#         },
#     },
# }

SWAGGER_SETTINGS = {
    'LOGIN_URL': '/admin/',
    'LOGOUT_URL': '/admin/',
    'USE_SESSION_AUTH': True,
    'SECURITY_DEFINITIONS': {
        'Basic': {
            'type': 'basic'
        },
        'Bearer': {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header'
        }
    },
    # 'SECURITY_REQUIREMENTS': {
    #     'Bearer': {
    #         "api_key": {
    #             'type': 'apiKey',
    #             'name': 'Authorization',
    #             'in': 'header'
    #         }
    #     }}
}

# Activate Django-Heroku.
import django_heroku

# args:logging=False :: do not over-write my logging in this settings
django_heroku.settings(locals(), databases=False, staticfiles=False, logging=False)
