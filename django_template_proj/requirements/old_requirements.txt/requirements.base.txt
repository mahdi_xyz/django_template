django==3.0.*; python_version>='3.7'
djangorestframework==3.11.*

# postgresql
psycopg2

# gevent required for celery and gunicorn
celery[redis]


# for chat app
channels
channels_redis


# redis cache
django-redis

# swagger documentation
drf_yasg

# google sign in
requests
google-auth-oauthlib

