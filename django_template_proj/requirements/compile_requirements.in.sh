#! /bin/sh

pip-compile $DJANGO_PROJECT/requirements/requirements.in/requirements.base.in -o $DJANGO_PROJECT/requirements/requirements.txt/requirements.base.txt && \
pip-compile $DJANGO_PROJECT/requirements/requirements.in/requirements.dev.in -o $DJANGO_PROJECT/requirements/requirements.txt/requirements.dev.txt && \
pip-compile $DJANGO_PROJECT/requirements/requirements.in/requirements.heroku.in -o $DJANGO_PROJECT/requirements/requirements.txt/requirements.heroku.txt && \
pip-compile $DJANGO_PROJECT/requirements/requirements.in/requirements.prod.in -o $DJANGO_PROJECT/requirements/requirements.txt/requirements.prod.txt && \
pip-compile $DJANGO_PROJECT/requirements/requirements.in/requirements.test.in -o $DJANGO_PROJECT/requirements/requirements.txt/requirements.test.txt
