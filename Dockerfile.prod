FROM python:3.8
LABEL MAINTAINER="mahdi.hamzeh.14@gmail.com"
### build context ###
# As the context of docker build command is like root (work dir) of the project and
# can't do any thing out of it (forbiden path error)

### .dockerignore ###
# .dockerignore must be in build context to work

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

#user
ENV DJANGO_USER=django_web
ENV DJANGO_GROUP=django_web
# create the appropriate directories
ENV DJANGO_PROJECT_BASE=/opt
ENV DJANGO_PROJECT_NAME=django_template_proj
ENV DJANGO_PROJECT=$DJANGO_PROJECT_BASE/$DJANGO_PROJECT_NAME

# first use the /etc/opt/$DJANGO_PROJECT_NAME to find the settings for django
ENV PYTHONPATH=/etc/opt/$DJANGO_PROJECT_NAME:$DJANGO_PROJECT

# set DOCKER default to run commands in.
SHELL ["/bin/bash", "-c"]

# directory and copy files
RUN mkdir -vp $DJANGO_PROJECT
WORKDIR $DJANGO_PROJECT
COPY . $DJANGO_PROJECT

# create user, make opt, log, setting directories
RUN mkdir -vp /var/opt/$DJANGO_PROJECT_NAME && \
    adduser --system --home=/var/opt/$DJANGO_PROJECT_NAME --no-create-home --disabled-password --group --shell=/bin/bash $DJANGO_USER && \

    chown $DJANGO_USER /var/opt/$DJANGO_PROJECT_NAME && \

    mkdir -vp /var/log/$DJANGO_PROJECT_NAME && \
    chown $DJANGO_USER /var/log/$DJANGO_PROJECT_NAME && \

    mkdir /etc/opt/$DJANGO_PROJECT_NAME && \

    cp $DJANGO_PROJECT/$DJANGO_PROJECT_NAME/settings/production.py /etc/opt/$DJANGO_PROJECT_NAME && \

    chgrp $DJANGO_GROUP /etc/opt/$DJANGO_PROJECT_NAME && \
    chmod u=rwx,g=rx,o= /etc/opt/$DJANGO_PROJECT_NAME && \

    apt-get update && \
    apt-get full-upgrade -y --no-install-recommends && \
    python3 -m pip install --upgrade pip && \

#    pycharm dose not support venv in docker containner
    python3 -m venv --system-site-package $DJANGO_PROJECT_BASE/venv/$DJANGO_PROJECT_NAME && \
    . $DJANGO_PROJECT_BASE/venv/$DJANGO_PROJECT_NAME/bin/activate && \

    python -m pip install --upgrade pip && \
    python -m pip install --no-cache-dir -r $DJANGO_PROJECT/requirements.prod.txt && \
    python -m pip install --no-cache-dir -r $DJANGO_PROJECT/requirements.dev.txt && \
    python -m pip install --no-cache-dir -r $DJANGO_PROJECT/requirements.test.txt && \
    python -m pip install --no-cache-dir -r $DJANGO_PROJECT/requirements.heroku.txt && \
    apt-get autoremove && \
    rm -rf /var/lib/apt/lists/* && \

    python -m compileall -f -j 2 && \
    python -m compileall -f /etc/opt/$DJANGO_PROJECT_NAME

    USER $DJANGO_USER


ENTRYPOINT ["./entrypoint.sh"]
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "django_template_proj.wsgi:application"]